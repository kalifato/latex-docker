FROM alpine:3.12

RUN apk update
RUN apk upgrade
RUN apk --no-cache add wget perl
RUN apk --no-cache add texlive-full biber

# To run online installer
# wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
# tar -xzf install-tl-unx.tar.gz
# cd install-tl-20*
# ./install-tl

USER 1000:1000

WORKDIR /tmp
