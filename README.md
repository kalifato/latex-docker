# Latex-Docker

This is a Docker file that will install Latex.

## Usage

```
$ docker build -t latex .
$ docker run --rm -v `pwd`:/tmp latex pdflatex sample.tex
```
